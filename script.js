'use strict';

///////////////////////////define variables////////////////
const player0 = document.querySelector('.player--0');
const player1 = document.querySelector('.player--1');
const score0El = document.querySelector('#score--0');
const score1El = document.querySelector('#score--1');
const currentP0 = document.querySelector('#current--0');
const currentP1 = document.querySelector('#current--1');
const dice = document.querySelector('.dice');
const newGame = document.querySelector('.btn--new');
const rolldice = document.querySelector('.btn--roll');
const holddice = document.querySelector('.btn--hold');
let scores,currentScore,activePlayer,playing;


const init = function(){
  
 scores = [0, 0];
score0El.textContent = 0;
score1El.textContent = 0;

 currentScore = 0;
 activePlayer = 0;
 playing = true;
  score0El.textContent = 0;
  score1El.textContent = 0;
  currentP1.textContent = 0;
  currentP0.textContent = 0;
  dice.classList.add('hidden');
  player0.classList.remove('player--winner');
  player1.classList.remove('player--winner');
  player0.classList.add('player--active');
  player1.classList.remove('player--active');
}
init();

const switchPlayer = () => {
  document.getElementById(`current--${activePlayer}`).textContent = 0;
  currentScore = 0;
  activePlayer = activePlayer === 0 ? 1 : 0;
  player0.classList.toggle('player--active');
  player1.classList.toggle('player--active');
};
///////////////////////////////////////add events
rolldice.addEventListener('click', function () {
  if (playing) {
    // 1.generating random number between 1 to 6
    const diceNum = Math.trunc(Math.random() * 6) + 1;
    console.log(diceNum);
    // 2.display dice
    dice.classList.remove('hidden');
    dice.src = `dice-${diceNum}.png`;
    //3. check for rolled 1: if true , switch to next player
    if (diceNum !== 1) {
      currentScore += diceNum;
      document.getElementById(`current--${activePlayer}`).textContent =
        currentScore;
    } else {
      switchPlayer();
    }
  }
});
holddice.addEventListener('click', function () {
  if (playing) {
    //1. add current player score to active player's score
    scores[activePlayer] = scores[activePlayer] + currentScore;
    console.log(activePlayer);
    console.log('holding the score', scores[activePlayer]);
    document.getElementById(`score--${activePlayer}`).textContent =
      scores[activePlayer];
    //2. check if player's score is >=100
    if (scores[activePlayer] >= 20) {
      playing = false;
      dice.classList.add('hidden');
      document
        .querySelector(`.player--${activePlayer}`)
        .classList.add('player--winner');
      document
        .querySelector(`.player--${activePlayer}`)
        .classList.remove('player--active');
    }
    // finish the game
    //switch to the next player
    else switchPlayer();
  }
});

newGame.addEventListener('click', init);
